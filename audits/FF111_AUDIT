# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `250178df19caa1fb25bfa0e35728426cfbde95f8` ( `FIREFOX_110_0_1_RELEASE` )
- End:   `431cede9cc9472bb648f5dfe24c54d0067c290e4`  ( `FIREFOX_111_0_1_RELEASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `5755d9ce30ef10248eb55c4b39a522a118ce7d95` ( `v97.1.0` )
- End:   `9657aebb7450c5b58e8b9a88bec12bd5e9e0f700`  ( `v97.2.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Firefox Android: https://github.com/mozilla-mobile/firefox-android.git

- Start: `bc529747751ab545dba0a90a339f11382d742c97`
- End:   `6b0f9fdb3f603974914de82185cd184065b2ebee`

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

#### Problematic Commits

- Bug 1811531 - Add 'site' query parameter to Pocket sponsored stories request
 `2dfe183ed96720b843f872fdf51fe206ed9a311c`
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41990
  - **RESOLUTION** pocket not available in PBM which we lock+enforce
- Bug 1812518 - Allow a custom View for 3rd party downloads `174237dcbb6d8c631f5834ecbc7875d670bb6d8d`
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41991
  - **RESOLUTION** feature disabled

## Fenix: https://github.com/mozilla-mobile/fenix.git

- Start: `69e08e3ffbf4b9f5a2ffec99b238fa0c5da1d315`
- End:   `8571b648efc860bd2511183540b792e10c6cc0f4`

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

#### Problematic Commits

- Bug 1815623 - Add telemetry for sharing to an app from the share sheet
 `4e5d9b323b465a73d24f8ee91a8b55df10545c36`

## Ticket Review ##

Bugzilla Query: `https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=111%20Branch&order=priority%2Cbug_severity&limit=0`

#### Problematic Tickets

- **Add API for saving a PDF** https://bugzilla.mozilla.org/show_bug.cgi?id=1810761
  - https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41993
  - **RESOLUTION** just enabling download byte-per-byte and viewing of PDF in browser, no extra meta data or conversion happens

## Export
- [x] Export Report and save to `tor-browser-spec/audits`