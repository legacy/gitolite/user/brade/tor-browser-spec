Tracking issue: https://gitlab.torproject.org/tpo/applications/tor-browser-spec/-/issues/40037

# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

`code_audit.sh` contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox: https://github.com/mozilla/gecko-dev.git

- Start: `856b9168439ef597dbd103cd1e2940a8ad110450` ( `FIREFOX_RELEASE_102_BASE` )
- End:   `4960b7d420528392cc095c247a662670785b18b9` ( `FIREFOX_RELEASE_103_BASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

---

## Application Services: https://github.com/mozilla/application-services.git

- Start: `0302b89604bb29adb34fdcd710feabd3dd01992d` ( `v93.5.0` )
- End:   `55cbbddfdcb4ec82d2850e0811e8675fea2686c2`  ( `v93.7.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Android Components: https://github.com/mozilla-mobile/android-components.git

- Start: `2b414097d4f540948f67f62f57c5ddcb0e2789d9` ( `v102.0.1` )
- End:   `cd19f9a6c5e26c4e57dda6e549a5c63ac7c042ea`  ( `v102.0.14` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Fenix: https://github.com/mozilla-mobile/fenix.git

- Start: `cc68c965cbb29eb16244d242d433051327de5f48` ( `v102.0.0-beta.1` )
- End:   `2ec252d5f5d09b3eb73840ce585453b7105a7a7d`  ( `releases_v102.0.0` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Ticket Review ##

### 102 https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=102%20Branch&order=priority%2Cbug_severity&limit=0

- https://bugzilla.mozilla.org/show_bug.cgi?id=1767919 : @pierov https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41152
  - nothing for us to do here
- ~~https://bugzilla.mozilla.org/show_bug.cgi?id=1770881 : @pierov https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41153~~ 102esr is unaffected: the Bugzilla ticket was wrong and then has been fixed
- https://bugzilla.mozilla.org/show_bug.cgi?id=1765167 : @pierov https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41154
  - no security/privacy implications, just a refactor required to maintain functionality
- https://bugzilla.mozilla.org/show_bug.cgi?id=1751450 : @richard https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41155
  - nothing for us to do here
