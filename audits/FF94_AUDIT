Tracking Issue: https://gitlab.torproject.org/tpo/applications/tor-browser-spec/-/issues/40023

# General

The audit begins at the commit hash where the previous audit ended. Use code_audit.sh for creating the diff and highlighting potentially problematic code. The audit is scoped to a specific language (currently C/C++, Rust, Java/Kotlin, and Javascript).

The output includes the entire patch where the new problematic code was introduced. Search for `XXX MATCH XXX` to find the next potential violation.

code_audit.sh contains the list of known problematic APIs. New usage of these functions are documented and analyzed in this audit.

## Firefox

### Repo: https://github.com/mozilla/gecko-dev.git

- Start: `5f4358c1c5bc2ca87d60eadebeab439562c90495` ( `FIREFOX_RELEASE_94_BASE` )
- End:   `6c9b6e1483551f220cd409e4e584349bc74a8231`  ( `FIREFOX_RELEASE_95_BASE` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

(mostly) only tests triggered matches or false positives

---

## Application Services

### Repo: https://github.com/mozilla/application-services.git

- Start: `b1f371719ca20db642b64a0e860b4ecb0aaf316f` ( v86.1.0 )
- End: `df1a47fde89f49201b1e839f960e8f16eb95a55d`  ( v87.1.0 )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

## Android Components

### Repo: https://github.com/mozilla-mobile/android-components.git

- Start: `fce7eb5cff2d56acd3195bf1d9a89386c63dc3d5` ( `v94.0.0` )
- End:   `28c1b7db40105dcaea09caa0b5108554a83959cd`  ( `v94.0.15` )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Fenix

### Repo: https://github.com/mozilla-mobile/fenix.git

- Start: 54d80751bfc9a4aa4341e78221060940a36e3d17 ( v94.0.0-beta.1 )
- End:   cb5708f88847601426833067f93d16d25d36451f  ( v94.1.2 )

### Languages:
- [x] java
- [x] cpp
- [x] js
- [x] rust

Nothing of interest (using `code_audit.sh`)

## Ticket Review ##

### Review List

#### 94 https://bugzilla.mozilla.org/buglist.cgi?query_format=advanced&resolution=FIXED&target_milestone=94%20Branch&order=priority%2Cbug_severity&limit=0

- https://bugzilla.mozilla.org/show_bug.cgi?id=1730418 : @ma1 https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41123
  - Nothing to do here
- https://bugzilla.mozilla.org/show_bug.cgi?id=1732388: @dan https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41124
  - Disabled by network.proxy.allow_bypass = false

